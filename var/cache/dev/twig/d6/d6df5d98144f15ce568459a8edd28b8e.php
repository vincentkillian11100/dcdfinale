<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* carte/indexAdmin.html.twig */
class __TwigTemplate_6c6519be7ac857bbf3e59ec034847ae9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "carte/indexAdmin.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "carte/indexAdmin.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "carte/indexAdmin.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Carte index";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo " <link rel=\"stylesheet\" href=\"/style/style.css\">
 ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo twig_include($this->env, $context, "header_admin/index.html.twig");
        echo "
    

 <h2 id=\"titreCarte2\"> adm66inistrateur</h2>
    <main class=\"mainAdmin\">
        <div class=\"boxcarte\">
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cartes"]) || array_key_exists("cartes", $context) ? $context["cartes"] : (function () { throw new RuntimeError('Variable "cartes" does not exist.', 16, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["carte"]) {
            // line 17
            echo "            <div class=\"burger avant\">
                <div class=\"imgcarte\">
                    <img src=\"";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["carte"], "image", [], "any", false, false, false, 19), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["carte"], "plat", [], "any", false, false, false, 19), "html", null, true);
            echo "\">
                </div>
                <div class=\"txtcarte\">
                    <h3>";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["carte"], "plat", [], "any", false, false, false, 22), "html", null, true);
            echo "</h3>
                    <h4>
                        ";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["carte"], "description", [], "any", false, false, false, 24), "html", null, true);
            echo "
                    </h4>
                    <h3 class=\"prix\">";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["carte"], "prix", [], "any", false, false, false, 26), "html", null, true);
            echo "€</h3>
                    <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_carte_show", ["id" => twig_get_attribute($this->env, $this->source, $context["carte"], "id", [], "any", false, false, false, 27)]), "html", null, true);
            echo "\">Regarder</a>
                    <a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_carte_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["carte"], "id", [], "any", false, false, false, 28)]), "html", null, true);
            echo "\">modifier</a>
                    
                </div>
            </div>
             ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['carte'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "             </div>
             <div class=\"containerLeftAdmin\">
                ";
        // line 35
        if (array_key_exists("new", $context)) {
            // line 36
            echo "                    ";
            $this->loadTemplate("carte/_form.html.twig", "carte/indexAdmin.html.twig", 36)->display($context);
            // line 37
            echo "                ";
        } else {
            // line 38
            echo "                    <a class=\"creatCarte\"href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_carte_new");
            echo "\">Crée une carte</a>
                ";
        }
        // line 40
        echo "               </div>
    </main>
   <a class=\"btnResa\" href=\"/reservation/admin\">Mes réservation</a>
<script src=\"/script/header.js\"></script>

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "carte/indexAdmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 40,  176 => 38,  173 => 37,  170 => 36,  168 => 35,  164 => 33,  153 => 28,  149 => 27,  145 => 26,  140 => 24,  135 => 22,  127 => 19,  123 => 17,  119 => 16,  110 => 10,  100 => 9,  89 => 5,  79 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Carte index{% endblock %}
{% block stylesheets %}
 <link rel=\"stylesheet\" href=\"/style/style.css\">
 {% endblock %}
 

{% block body %}
{{ include('header_admin/index.html.twig') }}
    

 <h2 id=\"titreCarte2\"> adm66inistrateur</h2>
    <main class=\"mainAdmin\">
        <div class=\"boxcarte\">
        {% for carte in cartes %}
            <div class=\"burger avant\">
                <div class=\"imgcarte\">
                    <img src=\"{{ carte.image }}\" alt=\"{{ carte.plat }}\">
                </div>
                <div class=\"txtcarte\">
                    <h3>{{ carte.plat }}</h3>
                    <h4>
                        {{ carte.description }}
                    </h4>
                    <h3 class=\"prix\">{{ carte.prix }}€</h3>
                    <a href=\"{{ path('app_carte_show', {'id': carte.id}) }}\">Regarder</a>
                    <a href=\"{{ path('app_carte_edit', {'id': carte.id}) }}\">modifier</a>
                    
                </div>
            </div>
             {% endfor %}
             </div>
             <div class=\"containerLeftAdmin\">
                {% if new is defined %}
                    {% include 'carte/_form.html.twig' %}
                {% else %}
                    <a class=\"creatCarte\"href=\"{{ path('app_carte_new') }}\">Crée une carte</a>
                {% endif %}
               </div>
    </main>
   <a class=\"btnResa\" href=\"/reservation/admin\">Mes réservation</a>
<script src=\"/script/header.js\"></script>

{% endblock %}
", "carte/indexAdmin.html.twig", "/home/killian/synfV12/templates/carte/indexAdmin.html.twig");
    }
}
