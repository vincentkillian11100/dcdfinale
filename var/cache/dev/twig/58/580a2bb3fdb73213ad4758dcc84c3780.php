<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* header_customer/index.html.twig */
class __TwigTemplate_cf8b051335fd54e10ca46688c4ddb59c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "header_customer/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "header_customer/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "header_customer/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello HeaderCustomerController!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo " <link rel=\"stylesheet\" href=\"/style/headerClient.css\">
 ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 8
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 9
        echo " <header>
        <div id=\"containerHautH\">
            
            <div id=\"containerLogoH\" class=\"logoHe\">
                <div class=\"l l1\"></div>
                <div class=\"l l2\"></div>
                <div class=\"l l3\"></div>
                <div class=\"l l4\"></div>
            </div>
            <div id=\"containerLogoH2\" class=\"logoHe2 nav-phone\">
                <div class=\"l l1\"></div>
                <div class=\"l l2\"></div>
                <div class=\"l l3\"></div>
                <div class=\"l l4\"></div>
            </div>
            <div id=\"containerTitreH\">
                <h1 id=\"titreH\"> Dead Cow Dinner </h1>
            </div>
        </div>
        <div class=\"containerANim\" id=\"containerBasH\">
            <nav class=\"navOrdi\">
                <ul class=\"ulH ulHmob\" id=\"ulH\">
                    <li>
                        <a class=\"Nav_Page_i\" href=\"/carte/client\">Notre Carte</a>
                    </li>
                    <li>
                        <a class=\"Nav_Page_i\" href=\"/\">Notre Restaurant</a>
                    </li>
                    <li>
                        <a class=\"Nav_Page_i\" href=\"/reservation/new\">Réserver une table</a>
                    </li>
                    <li>
                        <a class=\"Nav_Page_i\" id=\"Uber\" href=\"/\">Commander chez Uber Eat</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
    <script src=\"/script/header.js\"></script>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "header_customer/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 9,  100 => 8,  89 => 5,  79 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello HeaderCustomerController!{% endblock %}
 {% block stylesheets %}
 <link rel=\"stylesheet\" href=\"/style/headerClient.css\">
 {% endblock %}

{% block body %}
 <header>
        <div id=\"containerHautH\">
            
            <div id=\"containerLogoH\" class=\"logoHe\">
                <div class=\"l l1\"></div>
                <div class=\"l l2\"></div>
                <div class=\"l l3\"></div>
                <div class=\"l l4\"></div>
            </div>
            <div id=\"containerLogoH2\" class=\"logoHe2 nav-phone\">
                <div class=\"l l1\"></div>
                <div class=\"l l2\"></div>
                <div class=\"l l3\"></div>
                <div class=\"l l4\"></div>
            </div>
            <div id=\"containerTitreH\">
                <h1 id=\"titreH\"> Dead Cow Dinner </h1>
            </div>
        </div>
        <div class=\"containerANim\" id=\"containerBasH\">
            <nav class=\"navOrdi\">
                <ul class=\"ulH ulHmob\" id=\"ulH\">
                    <li>
                        <a class=\"Nav_Page_i\" href=\"/carte/client\">Notre Carte</a>
                    </li>
                    <li>
                        <a class=\"Nav_Page_i\" href=\"/\">Notre Restaurant</a>
                    </li>
                    <li>
                        <a class=\"Nav_Page_i\" href=\"/reservation/new\">Réserver une table</a>
                    </li>
                    <li>
                        <a class=\"Nav_Page_i\" id=\"Uber\" href=\"/\">Commander chez Uber Eat</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
    <script src=\"/script/header.js\"></script>
{% endblock %}
", "header_customer/index.html.twig", "/home/killian/synfV12/templates/header_customer/index.html.twig");
    }
}
