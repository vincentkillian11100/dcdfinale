<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* histoire/index.html.twig */
class __TwigTemplate_05ef73959ef30a101790f9fbc9508840 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "histoire/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "histoire/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "histoire/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello HistoireController!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "        <link rel=\"stylesheet\" href=\"/style/style.css\">
        ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo twig_include($this->env, $context, "header_customer/index.html.twig");
        echo "

 <main>
         <a href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
        echo "\">Retour</a> 
        <h1>Notre Histoire</h1>
        <div class=\"boxstory\">
            <div class=\"story left\">
                <div class=\"imgstory\" id=\"p1\">
                    <img src=\"./image/s1.webp\" alt=\"photo1\">
                </div>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo quod dolor totam alias excepturi
                    distinctio nihil ex officiis recusandae enim quisquam eos, molestias ab sapiente aspernatur. Fuga
                    tempore quam eum perferendis similique pariatur molestiae commodi sunt reprehenderit molestias. Rem
                    sint, ipsum repellat sapiente eos unde similique. Odit quasi blanditiis quaerat omnis nostrum velit,
                    animi natus corporis adipisci voluptatum in, fugit, distinctio deleniti mollitia? Ab ipsa, labore
                    accusantium exercitationem explicabo voluptas rerum blanditiis suscipit nisi quod atque aliquam
                    perspiciatis aut quae laboriosam. Velit aspernatur similique, consectetur id laboriosam sunt natus
                    eveniet pariatur iure exercitationem sit recusandae sapiente fugiat. Alias, aliquam aperiam.</p>
            </div>
            <div class=\"story right\">
                <div class=\"imgstory\" id=\"p2\">
                    <img src=\"./image/s2.png\" alt=\"photo2\">
                </div>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Velit doloribus pariatur ut fugiat
                    temporibusn
                    quaerat amet illum mollitia, sequi debitis quam optio blanditiis, quis facere. Quia blanditiis
                    molestiae
                    minus earum aliquid necessitatibus modi sit et cum, aut atque voluptatum qui eveniet soluta
                    dignissimos
                    consequuntur exercitationem, ad neque laborum magni. Dolor deserunt eius enim, reiciendis placeat
                    blanditiis minus veritatis et, ipsam, facilis vel quos reprehenderit aliquam consequuntur
                    voluptatum.
                    Nihil omnis fugiat facere! Deserunt voluptates amet incidunt saepe odio facere officia unde rem
                    eveniet?
                    Delectus beatae fugiat magni doloribus minima accusantium dolor alias facilis, tenetur libero vel
                    blanditiis aut at consequuntur ad!</p>
            </div>
            <div class=\"story left\">
                <div class=\"imgstory\" id=\"p3\">
                    <img src=\"./image/s3.png\" alt=\"photo3\">
                </div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores tempore, illum distinctio accusamus
                    reiciendis ullam doloribus ea, enim amet quasi laudantium sed unde quis quo quisquam non magnam
                    libero.
                    Nesciunt officia maiores natus quasi, perspiciatis eaque deserunt iste fugit ipsam! Rerum
                    repellendus
                    dolores earum quisquam reprehenderit culpa, quo odit architecto placeat saepe minima vitae
                    asperiores
                    ipsum explicabo non nesciunt dolor quidem dignissimos. Iure eveniet mollitia perferendis ducimus
                    facere
                    aspernatur. Dolorem consequatur nobis ex ad maxime vel obcaecati commodi doloribus corporis quis,
                    natus
                    at aliquam odio ut nesciunt quia accusamus eveniet. Sequi repudiandae neque quibusdam iure!
                    Repellendus
                    dolorem earum esse molestiae!</p>
            </div>
            <div class=\"story right\">
                <div class=\"imgstory\" id=\"p4\">
                    <img src=\"./image/s4.png\" alt=\"photo4\">
                </div>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente, neque. Dolor quas ut dolorem
                    assumenda, quae ipsam perferendis incidunt, voluptatum, commodi rem maiores? Incidunt consequuntur
                    sit,
                    fuga accusamus voluptate amet, expedita consequatur pariatur asperiores accusantium debitis non rem
                    assumenda aut dolorum, fugit illum cupiditate veniam nulla autem deleniti officiis doloribus? Neque
                    odit
                    aperiam suscipit amet corporis numquam rem eaque tempora esse? Ducimus quidem ab deleniti incidunt.
                    Pariatur, eligendi architecto quam consectetur numquam nesciunt ipsam officiis ullam maxime ea
                    quidem,
                    praesentium molestiae facere libero rem! Natus reprehenderit perspiciatis quidem, quisquam facilis
                    eos
                    dicta amet iste, excepturi distinctio esse asperiores doloremque! Voluptatem.</p>
            </div>
            <div class=\"story left\">
                <div class=\"imgstory\" id=\"p5\">
                    <img src=\"./image/s5.png\" alt=\"photo5\">
                </div>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tenetur dolore quisquam assumenda in?
                    Assumenda
                    unde ab, libero voluptate quos itaque nemo iusto neque perferendis odio id animi praesentium sed
                    corporis in. Nesciunt quas libero maiores quaerat iure debitis a dolor vel quis eaque expedita
                    impedit,
                    sint, id facere officiis obcaecati nam ullam, iste ipsam? Sint cupiditate expedita vero, totam amet
                    inventore, quae excepturi magnam dolore numquam unde dignissimos tempora! Repudiandae error magnam
                    quis
                    quisquam, natus minus modi, dolore deserunt perferendis odio rerum ipsum obcaecati vero? Iste
                    dolorum
                    dolorem hic deserunt? Inventore aspernatur eos necessitatibus, in amet debitis explicabo corrupti
                    at.
                </p>
            </div>
        </div>
    </main>

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "histoire/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 11,  110 => 8,  100 => 7,  89 => 5,  79 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello HistoireController!{% endblock %}
{% block stylesheets %}
        <link rel=\"stylesheet\" href=\"/style/style.css\">
        {% endblock %}
{% block body %}
{{ include('header_customer/index.html.twig') }}

 <main>
         <a href=\"{{path('app_home')}}\">Retour</a> 
        <h1>Notre Histoire</h1>
        <div class=\"boxstory\">
            <div class=\"story left\">
                <div class=\"imgstory\" id=\"p1\">
                    <img src=\"./image/s1.webp\" alt=\"photo1\">
                </div>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo quod dolor totam alias excepturi
                    distinctio nihil ex officiis recusandae enim quisquam eos, molestias ab sapiente aspernatur. Fuga
                    tempore quam eum perferendis similique pariatur molestiae commodi sunt reprehenderit molestias. Rem
                    sint, ipsum repellat sapiente eos unde similique. Odit quasi blanditiis quaerat omnis nostrum velit,
                    animi natus corporis adipisci voluptatum in, fugit, distinctio deleniti mollitia? Ab ipsa, labore
                    accusantium exercitationem explicabo voluptas rerum blanditiis suscipit nisi quod atque aliquam
                    perspiciatis aut quae laboriosam. Velit aspernatur similique, consectetur id laboriosam sunt natus
                    eveniet pariatur iure exercitationem sit recusandae sapiente fugiat. Alias, aliquam aperiam.</p>
            </div>
            <div class=\"story right\">
                <div class=\"imgstory\" id=\"p2\">
                    <img src=\"./image/s2.png\" alt=\"photo2\">
                </div>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Velit doloribus pariatur ut fugiat
                    temporibusn
                    quaerat amet illum mollitia, sequi debitis quam optio blanditiis, quis facere. Quia blanditiis
                    molestiae
                    minus earum aliquid necessitatibus modi sit et cum, aut atque voluptatum qui eveniet soluta
                    dignissimos
                    consequuntur exercitationem, ad neque laborum magni. Dolor deserunt eius enim, reiciendis placeat
                    blanditiis minus veritatis et, ipsam, facilis vel quos reprehenderit aliquam consequuntur
                    voluptatum.
                    Nihil omnis fugiat facere! Deserunt voluptates amet incidunt saepe odio facere officia unde rem
                    eveniet?
                    Delectus beatae fugiat magni doloribus minima accusantium dolor alias facilis, tenetur libero vel
                    blanditiis aut at consequuntur ad!</p>
            </div>
            <div class=\"story left\">
                <div class=\"imgstory\" id=\"p3\">
                    <img src=\"./image/s3.png\" alt=\"photo3\">
                </div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores tempore, illum distinctio accusamus
                    reiciendis ullam doloribus ea, enim amet quasi laudantium sed unde quis quo quisquam non magnam
                    libero.
                    Nesciunt officia maiores natus quasi, perspiciatis eaque deserunt iste fugit ipsam! Rerum
                    repellendus
                    dolores earum quisquam reprehenderit culpa, quo odit architecto placeat saepe minima vitae
                    asperiores
                    ipsum explicabo non nesciunt dolor quidem dignissimos. Iure eveniet mollitia perferendis ducimus
                    facere
                    aspernatur. Dolorem consequatur nobis ex ad maxime vel obcaecati commodi doloribus corporis quis,
                    natus
                    at aliquam odio ut nesciunt quia accusamus eveniet. Sequi repudiandae neque quibusdam iure!
                    Repellendus
                    dolorem earum esse molestiae!</p>
            </div>
            <div class=\"story right\">
                <div class=\"imgstory\" id=\"p4\">
                    <img src=\"./image/s4.png\" alt=\"photo4\">
                </div>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente, neque. Dolor quas ut dolorem
                    assumenda, quae ipsam perferendis incidunt, voluptatum, commodi rem maiores? Incidunt consequuntur
                    sit,
                    fuga accusamus voluptate amet, expedita consequatur pariatur asperiores accusantium debitis non rem
                    assumenda aut dolorum, fugit illum cupiditate veniam nulla autem deleniti officiis doloribus? Neque
                    odit
                    aperiam suscipit amet corporis numquam rem eaque tempora esse? Ducimus quidem ab deleniti incidunt.
                    Pariatur, eligendi architecto quam consectetur numquam nesciunt ipsam officiis ullam maxime ea
                    quidem,
                    praesentium molestiae facere libero rem! Natus reprehenderit perspiciatis quidem, quisquam facilis
                    eos
                    dicta amet iste, excepturi distinctio esse asperiores doloremque! Voluptatem.</p>
            </div>
            <div class=\"story left\">
                <div class=\"imgstory\" id=\"p5\">
                    <img src=\"./image/s5.png\" alt=\"photo5\">
                </div>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tenetur dolore quisquam assumenda in?
                    Assumenda
                    unde ab, libero voluptate quos itaque nemo iusto neque perferendis odio id animi praesentium sed
                    corporis in. Nesciunt quas libero maiores quaerat iure debitis a dolor vel quis eaque expedita
                    impedit,
                    sint, id facere officiis obcaecati nam ullam, iste ipsam? Sint cupiditate expedita vero, totam amet
                    inventore, quae excepturi magnam dolore numquam unde dignissimos tempora! Repudiandae error magnam
                    quis
                    quisquam, natus minus modi, dolore deserunt perferendis odio rerum ipsum obcaecati vero? Iste
                    dolorum
                    dolorem hic deserunt? Inventore aspernatur eos necessitatibus, in amet debitis explicabo corrupti
                    at.
                </p>
            </div>
        </div>
    </main>

{% endblock %}
", "histoire/index.html.twig", "/home/killian/synfV12/templates/histoire/index.html.twig");
    }
}
