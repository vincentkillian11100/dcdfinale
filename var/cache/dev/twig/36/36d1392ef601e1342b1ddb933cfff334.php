<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/index.html.twig */
class __TwigTemplate_1bf22da042fb44f96c2a84401f822ac2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello IndexControlController!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo " <link rel=\"stylesheet\" href=\"/style/style.css\">
 

 ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 12
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo twig_include($this->env, $context, "header_customer/index.html.twig");
        echo "
<main class=\"mainG\">
<div class=\"example-wrapper\">
   
    <!-- commander uber (version telephone) -->
    <div class=\"commander\">
        <p class=\"textcommander\">Commander avec Uber Eats</p>
    </div>
    <!-- titre 2 -->
    <div>
        <h2>notre Restaurant</h2>
    </div>

    <!-- conteneur 1 -->
    <div class=\"slider\">
        <!-- car1 -->
        <div class=\"slides\">
            <!-- img1 -->
            <div class=\"box-horaire\">
                <p class=\"hscipt\">
                    <spans id=\"heure\"></span>
                </p>
            </div>
        </div>
    </div>
    <!-- -400px -->
    <div class=\"box-horaire tel\">
        <p class=\"hscipt\">
            <spans id=\"heure1\"></span>
        </p>
    </div>
    <!-- btn telephone-->
    <a class=\"lien1 btnphone\" href=\"";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_histoire");
        echo "\">
        <div class=\"btn1 btnphone\">
            <div class=\"btn\">
                <p class=\"text_btn\">En savoir plus</p>
                <div id=\"triangle-code\">&nbsp;</div>
            </div>
        </div>
    </a>
    <!-- conteneur2 -->
    <div class=\"description1\">
        <p class=\"textdesc1\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius debitis ipsum enim excepturi
            aliquid, consequatur
            sint beatae, officiis rem laboriosam blanditiis numquam explicabo libero quas ad vel officia, eum deleniti?
        </p>
    </div>
    <!-- conteneur 3 -->
    <div class=\"bigbox\">
        <div class=\"partL\">
            <iframe
                src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d727.3254351033876!2d2.9787390179781608!3d43.18217917721107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sfr!2sfr!4v1637761494536!5m2!1sfr!2sfr\"
                width=\"600\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\"></iframe>
        </div>
        <div class=\"partR\">
            <div class=\"description2\">
                <p class=\"textdesc2\"> ipsum dolor sit amet consectetur adipisicing elit. Placeat molestias quisquam
                    ducimus
                    consectetur accusamuss cupiditate voluptas voluptates necessitatibus excepturi aspernatur eius, unde
                    minima laudantium assumenda esse numquam nemo impedit optio?</p>
            </div>
            <div class=\"box-LR\">
                <div class=\"box-adresse\">
                    <p class=\"adresse\">adresse 42 rue du lorem</p>
                    <p class=\"adresse\">34000</p>
                    <p class=\"adresse\">Horaire: 12H00 a 14H00</p>
                    <p class=\"adresse\">18H00 a 00H00</p>
                    <p class=\"adresse\">Touts les jours ouvert sauf mardi</p>
                    <a class=\"adresse\" href=\"tel:+33412345678\">04.12.34.56.78</a>
                </div>
                <div class=\"btn1\">
                    <div class=\"btn\">
                        <a class=\"lien1\" href=\"";
        // line 85
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_histoire");
        echo "\">
                            <p class=\"text_btn\">En savoir plus</p>
                            <div id=\"triangle-code\">&nbsp;</div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
<script src=\"/script/script.js\"></script>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 85,  147 => 45,  112 => 13,  102 => 12,  89 => 6,  79 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello IndexControlController!{% endblock %}

 {% block stylesheets %}
 <link rel=\"stylesheet\" href=\"/style/style.css\">
 

 {% endblock %}


{% block body %}
{{ include('header_customer/index.html.twig') }}
<main class=\"mainG\">
<div class=\"example-wrapper\">
   
    <!-- commander uber (version telephone) -->
    <div class=\"commander\">
        <p class=\"textcommander\">Commander avec Uber Eats</p>
    </div>
    <!-- titre 2 -->
    <div>
        <h2>notre Restaurant</h2>
    </div>

    <!-- conteneur 1 -->
    <div class=\"slider\">
        <!-- car1 -->
        <div class=\"slides\">
            <!-- img1 -->
            <div class=\"box-horaire\">
                <p class=\"hscipt\">
                    <spans id=\"heure\"></span>
                </p>
            </div>
        </div>
    </div>
    <!-- -400px -->
    <div class=\"box-horaire tel\">
        <p class=\"hscipt\">
            <spans id=\"heure1\"></span>
        </p>
    </div>
    <!-- btn telephone-->
    <a class=\"lien1 btnphone\" href=\"{{path('app_histoire')}}\">
        <div class=\"btn1 btnphone\">
            <div class=\"btn\">
                <p class=\"text_btn\">En savoir plus</p>
                <div id=\"triangle-code\">&nbsp;</div>
            </div>
        </div>
    </a>
    <!-- conteneur2 -->
    <div class=\"description1\">
        <p class=\"textdesc1\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius debitis ipsum enim excepturi
            aliquid, consequatur
            sint beatae, officiis rem laboriosam blanditiis numquam explicabo libero quas ad vel officia, eum deleniti?
        </p>
    </div>
    <!-- conteneur 3 -->
    <div class=\"bigbox\">
        <div class=\"partL\">
            <iframe
                src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d727.3254351033876!2d2.9787390179781608!3d43.18217917721107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sfr!2sfr!4v1637761494536!5m2!1sfr!2sfr\"
                width=\"600\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\"></iframe>
        </div>
        <div class=\"partR\">
            <div class=\"description2\">
                <p class=\"textdesc2\"> ipsum dolor sit amet consectetur adipisicing elit. Placeat molestias quisquam
                    ducimus
                    consectetur accusamuss cupiditate voluptas voluptates necessitatibus excepturi aspernatur eius, unde
                    minima laudantium assumenda esse numquam nemo impedit optio?</p>
            </div>
            <div class=\"box-LR\">
                <div class=\"box-adresse\">
                    <p class=\"adresse\">adresse 42 rue du lorem</p>
                    <p class=\"adresse\">34000</p>
                    <p class=\"adresse\">Horaire: 12H00 a 14H00</p>
                    <p class=\"adresse\">18H00 a 00H00</p>
                    <p class=\"adresse\">Touts les jours ouvert sauf mardi</p>
                    <a class=\"adresse\" href=\"tel:+33412345678\">04.12.34.56.78</a>
                </div>
                <div class=\"btn1\">
                    <div class=\"btn\">
                        <a class=\"lien1\" href=\"{{path('app_histoire')}}\">
                            <p class=\"text_btn\">En savoir plus</p>
                            <div id=\"triangle-code\">&nbsp;</div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
<script src=\"/script/script.js\"></script>
{% endblock %}", "home/index.html.twig", "/home/killian/synfV12/templates/home/index.html.twig");
    }
}
