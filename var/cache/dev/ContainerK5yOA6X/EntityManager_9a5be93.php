<?php

namespace ContainerK5yOA6X;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHoldera5a1f = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerf7db1 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties29270 = [
        
    ];

    public function getConnection()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getConnection', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getMetadataFactory', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getExpressionBuilder', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'beginTransaction', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getCache', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getCache();
    }

    public function transactional($func)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'transactional', array('func' => $func), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'wrapInTransaction', array('func' => $func), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'commit', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->commit();
    }

    public function rollback()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'rollback', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getClassMetadata', array('className' => $className), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'createQuery', array('dql' => $dql), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'createNamedQuery', array('name' => $name), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'createQueryBuilder', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'flush', array('entity' => $entity), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'clear', array('entityName' => $entityName), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->clear($entityName);
    }

    public function close()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'close', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->close();
    }

    public function persist($entity)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'persist', array('entity' => $entity), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'remove', array('entity' => $entity), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'refresh', array('entity' => $entity), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'detach', array('entity' => $entity), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'merge', array('entity' => $entity), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getRepository', array('entityName' => $entityName), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'contains', array('entity' => $entity), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getEventManager', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getConfiguration', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'isOpen', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getUnitOfWork', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getProxyFactory', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'initializeObject', array('obj' => $obj), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'getFilters', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'isFiltersStateClean', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'hasFilters', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return $this->valueHoldera5a1f->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerf7db1 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHoldera5a1f) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHoldera5a1f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHoldera5a1f->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, '__get', ['name' => $name], $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        if (isset(self::$publicProperties29270[$name])) {
            return $this->valueHoldera5a1f->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera5a1f;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera5a1f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera5a1f;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera5a1f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, '__isset', array('name' => $name), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera5a1f;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHoldera5a1f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, '__unset', array('name' => $name), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera5a1f;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHoldera5a1f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, '__clone', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        $this->valueHoldera5a1f = clone $this->valueHoldera5a1f;
    }

    public function __sleep()
    {
        $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, '__sleep', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;

        return array('valueHoldera5a1f');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerf7db1 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerf7db1;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerf7db1 && ($this->initializerf7db1->__invoke($valueHoldera5a1f, $this, 'initializeProxy', array(), $this->initializerf7db1) || 1) && $this->valueHoldera5a1f = $valueHoldera5a1f;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldera5a1f;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHoldera5a1f;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
