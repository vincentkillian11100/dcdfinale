<?php

namespace App\Form;

use App\Entity\Carte;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CarteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
             
            ->add('plat', TextType::class, [
                'attr' => [
                    'min' => '2',
                    'max' => '100'
                ]
            ])
            ->add('prix', TextType::class, [
                'attr' => [
                    'min' => '2',
                    'max' => '100'
                ],
                'constraints' => [
                    new Assert\Positive(),
                    new Assert\Length(['min' => 2, 'max' => 100])
                ]
            ])
            ->add('description', TextareaType::class,[
                'attr' => [
                    'min' => '2',
                    'max' => '255'
                ],
                'constraints' => [
                    new Assert\Positive(),
                    new Assert\Length(['min' => 2, 'max' => 255])
                ]
            ])
            ->add('categorie', TextType::class,[
                'attr' => [
                    'min' => '2',
                    'max' => '255'
                ],
                'constraints' => [
                    new Assert\Positive(),
                    new Assert\Length(['min' => 2, 'max' => 100])
                ]
            ])
            ->add('image', TextType::class,[
                'attr' => [
                    'min' => '2',
                    'max' => '255'
                ],
                'constraints' => [
                    new Assert\Positive(),
                    new Assert\Length(['min' => 2, 'max' => 255])
                ]
            ])
            ->add('dispo')
         
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Carte::class,
        ]);
    }
}
