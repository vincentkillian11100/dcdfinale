<?php

namespace App\Form;

use App\Entity\Reservation;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'attr' => [
                    'class' => 'resNom',
                    'min' => '2',
                    'max' => '100'
                ],
                'label' => 'Nom',
                'label_attr' => [
                    'class' => 'form-label mt-4'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 2, 'max' => 100]),
                    new Assert\NotBlank()
                ]
            ])
            ->add('telephone', TelType::class, [
                'attr' => [
                    'class' => 'numeroMobile',
                    'min' => '10',
                    'max' => '10'
                ],
                'label' => 'Téléphone',
                'label_attr' => [
                    'class' => 'Tel'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 10, 'max' => 10]),
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ]])
            ->add('date', DateTimeType::class,[
                    
                'years' => [2022,2023],
                'placeholder' =>[
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                    'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
                ],
                'hours' => [12,13,14,19, 20, 21, 23,]
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
