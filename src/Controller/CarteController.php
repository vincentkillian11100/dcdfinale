<?php

namespace App\Controller;

use App\Entity\Carte;
use App\Form\CarteType;
use App\Repository\CarteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/carte')]
class CarteController extends AbstractController
{
    #[Route('/admin', name: 'app_carte_index', methods: ['GET'])]
    public function index(CarteRepository $carteRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('carte/indexAdmin.html.twig', [
            'cartes' => $carteRepository->findAll(),
        ]);
    }
    #[Route('/adminCreat', name: 'app_carte_indexCreat', methods: ['GET'])]
    public function indexCreat(CarteRepository $carteRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('carte/indexAdminCreat.html.twig', [
            'cartes' => $carteRepository->findAll(),
        ]);
    }
    #[Route('/client', name: 'adm_carte_index', methods: ['GET'])]
    public function indexAdmin(CarteRepository $carteRepository): Response
    {
        return $this->render('carte/indexClient.html.twig', [
            'cartes' => $carteRepository->findAll(),
        ]);
    }


    #[Route('/new', name: 'app_carte_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CarteRepository $carteRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $carte = new Carte();
        $form = $this->createForm(CarteType::class, $carte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $carteRepository->add($carte);
            return $this->redirectToRoute('app_carte_index', [], Response::HTTP_SEE_OTHER);
        }
        
        return $this->renderForm('carte/indexAdmin.html.twig', [
            'carte' => $carte,
            'form' => $form,
            'cartes' => $carteRepository->findAll(),
            'new' => true,

        ]);
        // return $this->renderForm('carte/new.html.twig', [
        //     'carte' => $carte,
        //     'form' => $form,
        // ]);
    }

    #[Route('/{id}', name: 'app_carte_show', methods: ['GET'])]
    public function show(Carte $carte): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('carte/show.html.twig', [
            'carte' => $carte,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_carte_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Carte $carte, CarteRepository $carteRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $form = $this->createForm(CarteType::class, $carte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $carteRepository->add($carte);
            return $this->redirectToRoute('app_carte_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('carte/edit.html.twig', [
            'carte' => $carte,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_carte_delete', methods: ['POST'])]
    public function delete(Request $request, Carte $carte, CarteRepository $carteRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        if ($this->isCsrfTokenValid('delete'.$carte->getId(), $request->request->get('_token'))) {
            $carteRepository->remove($carte);
        }

        return $this->redirectToRoute('app_carte_index', [], Response::HTTP_SEE_OTHER);
    }
}
